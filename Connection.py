class ConnectionForward:
  def __init__(self, current, next, inverted=False):
    self.curva = next
    self.bias = -1 if inverted else 1
    if(all([self.curva != connection.curva for connection in current.connectedForward])):
      current.connectedForward += [self]

class ConnectionBackward:
  def __init__(self, current, next, inverted=False):
    self.curva = next
    self.bias = -1 if inverted else 1
    if(all([self.curva != connection.curva for connection in current.connectedBackward])):
      current.connectedBackward += [self]
  