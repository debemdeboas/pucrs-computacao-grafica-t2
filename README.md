# [T2 - Computação Gráfica](https://gitlab.com/debemdeboas/pucrs-computacao-grafica-t2) - CC 2022/2 PUCRS

O trabalho foi desenvolvido em Python 3.10 em Windows 64-bit utilizando os pacotes não-oficiais
do `PyOpenGL` e `PyOpenGL_Accelerate`. Os arquivos `.whl` estão armazenados na pasta [`lib`](./lib).

Foi gravado um vídeo de demonstração do programa e feito o upload para o [YouTube](https://youtu.be/leOA89f2dBY).
O relatório escrito está [aqui](./Relatorio.md).

## Autores

Isabella Do Prado Pagnoncelli, Rafael Almeida de Bem.

## Instalação dos pacotes

```commandline
$ python3.10 -m venv venv
$ ./venv/Scripts/activate
(venv) $ pip install --no-index --find-links=lib/ -r requirements.txt
```
