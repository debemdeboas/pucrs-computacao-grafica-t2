from re import S
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from enum import Enum
from Ponto import *
import copy
import random
import math

class AIType(Enum):
  PLAYER = 1
  ENEMY = 2

CAR_BODY = [Ponto(-1, -1), Ponto(1, -1), Ponto(0, 2)]
SPEED = 0.1
SIZE = 1.5

class Speeds(Enum):
  STOP = 0
  FORWARD = 1
  BACKWARD = 2

class Car:
  def __init__(self, type = AIType.ENEMY):
    self.type = type
    self.curva = None
    self.next = None
    self.vertices = copy.deepcopy(CAR_BODY)
    self.position: float = 0
    self.length = 0
    self.angle = 0
    self.speed = 0
    self.lastSpeed = 0
    self.inScene = False

  def setStart(self, curva, position=0.0, forward=True):
    self.inScene = True
    self.curva = curva
    self.speed = SPEED if forward else -SPEED
    self.position = position
    if self.type == AIType.ENEMY: 
      self.curva.cars.add(self)

  def setSpeed(self, speed=Speeds.STOP):
    if(speed == Speeds.STOP and self.speed != 0):
      print("stop player")
      self.lastSpeed = self.speed
      self.speed = 0
      if(self.next):
        self.next.curva.selected = False
        self.next = None
    elif(speed == Speeds.FORWARD and self.speed == 0):
      print("player continue movement")
      self.speed = self.lastSpeed
    elif(speed == Speeds.BACKWARD and self.speed == 0):
      self.speed = -self.lastSpeed
      print("player revert movement")

  def setNext(self, connection):
    if(self.type == AIType.PLAYER): 
      if(self.next): self.next.curva.selected = False
      self.next = connection
      self.next.curva.selected = True
    else:
      self.next = connection

  def render(self):
    if(not self.inScene): return

    if(self.type == AIType.PLAYER): 
      glColor3d(0, 0.8, 0)
    else: 
      glColor3d(0.8, 0, 0)

    glPushMatrix()
    point = self.curva.getPonto(self.position)
    glTranslate(point.x, point.y, 0)

    glScale(SIZE, SIZE, 1)

    if(self.speed != 0):
      direction = self.curva.tangent(self.position).getAngle()
      self.angle = -(direction + (0 if (self.speed > 0) else 180))
    glRotate(self.angle, 0, 0, 1)

    glBegin(GL_POLYGON)
    [glVertex3f(p.x, p.y, 0) for p in self.vertices]
    glEnd()
    glPopMatrix()

  def move(self):
    if(self.speed == 0): return

    self.position += self.speed / self.curva.length
    self.length = self.position * self.curva.length

    # mudança de curva
    # se esta indo para frente
    if(self.speed > 0):
      # momento de escolher próxima curva
      if(self.position > 0.5 and self.next == None): 
        self.chooseNext()
      elif(self.position > 1):
        # momento de mover para próxima curva
        self.position = 0 if self.next.bias == 1 else 1
        self.changecurva()

    # se esta indo para tras
    else:
      # momento de escolher próxima curva
      if(self.position < 0.5 and self.next == None): 
        self.chooseNext()
      elif(self.position < 0):
        # momento de mover para próxima curva
        self.position = 1 if self.next.bias == 1 else 0
        self.changecurva()

  def changecurva(self):
    if(self.type == AIType.ENEMY): 
      self.curva.cars.remove(self)
      self.next.curva.cars.add(self)

    self.curva = self.next.curva
    self.speed *= self.next.bias
    self.next = None
    if(self.type == AIType.PLAYER): self.curva.selected = False

  def chooseNext(self):
    if(self.speed > 0):
      size = len(self.curva.connectedForward)
      next = random.randint(0, size-1)
      self.setNext(self.curva.connectedForward[next])
    else:
      size = len(self.curva.connectedBackward)
      next = random.randint(0, size-1)
      self.setNext(self.curva.connectedBackward[next])

  def trocarCurva(self, clockwise=True):
    if(self.next):
      print(f"cicle curva " + ("" if clockwise else "counter ") + "clockwise")
      current = 0
      size = 0
      if(self.speed > 0):
        size = len(self.curva.connectedForward)
        current = self.curva.connectedForward.index(self.next)

      else:
        size = len(self.curva.connectedBackward)
        current = self.curva.connectedBackward.index(self.next)
      
      current += 1 if clockwise else -1
      if(current >= size):
        current = 0
      elif(current < 0):
        current = size - 1

      if(self.speed > 0):
        self.setNext(self.curva.connectedForward[current])
      else:
        self.setNext(self.curva.connectedBackward[current])

  def __str__(self):
    return f"curva: {self.curva}\nNext: {self.next}\nPosition: {self.position}\nSpeed: {self.speed}\nType: {self.type}\nAngle: {self.angle}\nInScene: {self.inScene}"
