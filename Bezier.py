
import random

from Ponto import Ponto
from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *

class Bezier:
    # def __init__NEW(self, p0: Ponto, p1: Ponto, p2: Ponto):
    #     print("Construtora da Bezier")
    #     self.ComprimentoTotalDaCurva = 0.0
    #     self.Coords = []
    #     self.Coords += [p0]
    #     self.Coords += [p1]
    #     self.Coords += [p2]
    #     #P = self.Coords[0]
    #     # P.imprime()

    def __init__(self, pontos=[]):
        def get_color() -> float:
            while (color := random.random()) < 0.2:
                continue
            return color

        print("Construtora da Bezier")
        self.pontos = pontos
        self.selected = False
        self.order = len(self.pontos)
        self.length = 0
        self.color = (get_color(), get_color(), get_color())
        # calcula as coordenadas de bounding box
        self.connectedForward = []
        self.connectedBackward = []
        # referência de carros presentes na rua
        self.cars = set()
        self.calculateLength()

    def firstPoint(self): return self.pontos[0]
    def lastPoint(self): return self.pontos[self.order - 1]

    def tangent(self, p):
        x = 0
        y = 0
        if (self.order == 3):
            x = -2 * (1 - p) * self.pontos[0].x - 2 * p * self.pontos[1].x + \
                2 * (1 - p) * self.pontos[1].x + 2 * p * self.pontos[2].x
            y = -2 * (1 - p) * self.pontos[0].y - 2 * p * self.pontos[1].y + \
                2 * (1 - p) * self.pontos[1].y + 2 * p * self.pontos[2].y
        else:
            x = -3 * (1 - p)**2 * self.pontos[0].x + 3 * (1 - p)**2 * self.pontos[1].x - 6 * p * (
                1 - p) * self.pontos[1].x - 3 * p**2 * self.pontos[2].x + 6 * p * (1 - p) * self.pontos[2].x + 3 * p**2 * self.pontos[3].x
            y = -3 * (1 - p)**2 * self.pontos[0].y + 3 * (1 - p)**2 * self.pontos[1].y - 6 * p * (
                1 - p) * self.pontos[1].y - 3 * p**2 * self.pontos[2].y + 6 * p * (1 - p) * self.pontos[2].y + 3 * p**2 * self.pontos[3].y
        return Ponto(x, y)

    # esse trecho foi espelhado no site https://javascript.info/bezier-curve
    def getPonto(self, p):
        x = 0
        y = 0
        if (self.order == 3):
            x = (1 - p)**2 * self.pontos[0].x + 2 * (1 - p) * \
                p * self.pontos[1].x + p**2 * self.pontos[2].x
            y = (1 - p)**2 * self.pontos[0].y + 2 * (1 - p) * \
                p * self.pontos[1].y + p**2 * self.pontos[2].y
        else:
            x = (1 - p)**3 * self.pontos[0].x + 3 * (1 - p)**2 * p * self.pontos[1].x + 3 * (
                1 - p) * p**2 * self.pontos[2].x + p**3 * self.pontos[3].x
            y = (1 - p)**3 * self.pontos[0].y + 3 * (1 - p)**2 * p * self.pontos[1].y + 3 * (
                1 - p) * p**2 * self.pontos[2].y + p**3 * self.pontos[3].y
        return Ponto(x, y)

    def calculateLength(self):
        delta = 1.0 / 20
        posicao = delta
        P1 = self.getPonto(0.0)
        while(posicao < 1.0):
            P2 = self.getPonto(posicao)
            self.length += P1.distance(P2)
            P1 = P2
            posicao += delta

        P2 = self.getPonto(1.0)
        self.length += P1.distance(P2)

    def render(self):
        delta = 1.0 / 20
        position = delta
        glLineWidth(2)
        if(self.selected): 
            glColor3d(0.8, 0.8, 0)
        else: 
            glColor3d(*self.color)
        glBegin(GL_LINE_STRIP)
        point = self.getPonto(0.0)
        glVertex3f(point.x, point.y, 0)

        while(position < 1.0):
            point = self.getPonto(position)
            glVertex3f(point.x, point.y, 0)
            position += delta

        point = self.getPonto(1.0)
        glVertex3f(point.x, point.y, 0)
        glEnd()
