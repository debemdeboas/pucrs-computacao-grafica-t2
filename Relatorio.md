# Relatório - T2

O vídeo de demonstração está [aqui](https://youtu.be/leOA89f2dBY).

## Autores

Isabella Do Prado Pagnoncelli, Rafael Almeida de Bem.

## O Programa

Nosso programa lê as curvas e pontos que serão desenhados na tela de dois arquivos: `Curvas.txt` e `PontosCurvas.txt`.
Os veículos são triângulos preenchidos, sendo os veículos vermelhos os inimigos e o veículo verde o jogador.

O jogador pode ser parado usando a tecla <kbd>ESPAÇO</kbd>.
Também podemos controlar a velocidade do jogador. <kbd>W</kbd> ou <kbd>ARROW UP</kbd> aumenta a velocidade,
e <kbd>S</kbd> ou <kbd>ARROW DOWN</kbd> diminuem.
Podemos escolher a próxima curva utilizando <kbd>A</kbd> ou <kbd>ARROW LEFT</kbd> para escolhermos no sentido anti-horário e
<kbd>D</kbd> ou <kbd>ARROW RIGHT</kbd> para escolhermos no sentido horário.

Os veículos são instâncias de um mesmo objeto e não um modelo separado por veículo.
Não há cópias de modelos e sim referências. Todas as transformações são feitas pelo OpenGL e não
de forma manual.
