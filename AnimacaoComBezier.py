from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
from InstanciaBZ import *
from Bezier import *
from Car import *
import re
import random
from Connection import ConnectionBackward, ConnectionForward

# ***********************************************************************************

# Limites da Janela de Seleção
Min = Ponto()
Max = Ponto()
FPS = math.floor(1000/60)

# lista de instancias do Personagens
ENEMIES = 10
Player = Car(AIType.PLAYER)
Enemies = [Car() for _ in range(ENEMIES)]

# Lista de curvas Bezier
BzCurvas = []
# Lista de pontos para as curvas
Pontos = []

ESCAPE = b'\x1b'

# ***********************************************************************************

def reshape(w, h):
    glViewport(0, 0, w, h)
    glMatrixMode(GL_PROJECTION)
    glLoadIdentity()
    BordaX = abs(Max.x-Min.x)*0.1
    BordaY = abs(Max.y-Min.y)*0.1
    glOrtho(Min.x-BordaX, Max.x+BordaX, Min.y-BordaY, Max.y+BordaY, 0.0, 1.0)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()

# ***********************************************************************************

def getLimits(points):
    for point in points:
        if point.x > Max.x:
            Max.x = point.x
        if point.y > Max.y:
            Max.y = point.y
        if point.x < Min.x:
            Min.x = point.x
        if point.y < Min.y:
            Min.y = point.y

# ***********************************************************************************

def display():
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT)
    glMatrixMode(GL_MODELVIEW)
    glLoadIdentity()
    glColor3f(1, 1, 1)
    for curvas in BzCurvas:
        curvas.render()  # desenha as curvas
    Player.render()  # add o jogador
    for enemy in Enemies:
        enemy.render()  # add os inimigos
    glutSwapBuffers()

# ***********************************************************************************

def keyboard(*args):
    # print(args)
    # If escape is pressed, kill everything.
    if args[0] == b'q':
        os._exit(0)
    if args[0] == ESCAPE:
        os._exit(0)
    elif args[0] == b' ':
        Player.setSpeed()
    elif args[0] == b'w':
        Player.setSpeed(Speeds.FORWARD)
    elif args[0] == b's':
        Player.setSpeed(Speeds.BACKWARD)
    elif args[0] == b'a':
        Player.trocarCurva(False)
    elif args[0] == b'd':
        Player.trocarCurva(True)
    # Forca o redesenho da tela
    glutPostRedisplay()

# ***********************************************************************************

def arrow_keys(a_keys: int, x: int, y: int):
    print(a_keys, x, y)
    if a_keys == GLUT_KEY_UP:
        Player.setSpeed(Speeds.FORWARD)
    elif a_keys == GLUT_KEY_DOWN:
        Player.setSpeed(Speeds.BACKWARD)
    elif a_keys == GLUT_KEY_LEFT:
        Player.trocarCurva(False)
    elif a_keys == GLUT_KEY_RIGHT:
        Player.trocarCurva(True)
    # Forca o redesenho da tela
    glutPostRedisplay()

# ***********************************************************************************

def mouse(button: int, state: int, x: int, y: int):
    glutPostRedisplay()
    return
    # global PontoClicado
    # if (state != GLUT_DOWN):
    #     return
    # if (button != GLUT_RIGHT_BUTTON):
    #     return
    # Converte a coordenada de tela para o sistema de coordenadas do
    # Personagens definido pela glOrtho
    # vport = glGetIntegerv(GL_VIEWPORT)
    # mvmatrix = glGetDoublev(GL_MODELVIEW_MATRIX)
    # projmatrix = glGetDoublev(GL_PROJECTION_MATRIX)
    # realY = vport[3] - y
    # worldCoordinate1 = gluUnProject(x, realY, 0, mvmatrix, projmatrix, vport)
    # PontoClicado = Ponto(
    #     worldCoordinate1[0], worldCoordinate1[1], worldCoordinate1[2])
    # PontoClicado.imprime("Ponto Clicado:")
    # glutPostRedisplay()

# ***********************************************************************************

def verificaColisao():
    if (any([abs(car.length - Player.length) < 1 for car in Player.curva.cars])):
        print(f"Game over!")
        # os._exit(0)

# ***********************************************************************************

def CriaCurvas():
    for curvaAtual in BzCurvas:
        forward = curvaAtual.lastPoint()
        backward = curvaAtual.firstPoint()

        for proximaCurva in BzCurvas:
            nextForward = proximaCurva.lastPoint()
            nextBackward = proximaCurva.firstPoint()

            if (forward == nextBackward):
                ConnectionForward(curvaAtual, proximaCurva)
                ConnectionBackward(proximaCurva, curvaAtual)

            if (backward == nextForward):
                ConnectionBackward(curvaAtual, proximaCurva)
                ConnectionForward(proximaCurva, curvaAtual)

            if (curvaAtual != next):
                if (forward == nextForward):
                    ConnectionForward(curvaAtual, proximaCurva, True)
                    ConnectionForward(proximaCurva, curvaAtual, True)

                if (backward == nextBackward):
                    ConnectionBackward(curvaAtual, proximaCurva, True)
                    ConnectionBackward(proximaCurva, curvaAtual, True)

    for curves in BzCurvas:
        curves.connectedForward.sort(key=escolheProximaCurvaFrente)
        curves.connectedBackward.sort(key=escolheProximaCurvaAtras)

# ***********************************************************************************

def criaCurvasDeArquivo():
    infile = open("PontosCurvas.txt")
    for line in infile.readlines():
        global Pontos
        Pontos += [Ponto(x[1], x[2]) for x in [x.groups()
                                               for x in re.finditer(r'((-*\d+)\s(-*\d+))', line)]]
    getLimits(Pontos)
    infile.close()

    infile = open("Curvas.txt")
    for line in infile.readlines():
        global BzCurvas
        BzCurvas += [Bezier([Pontos[int(x[0])] for x in [x.groups()
                            for x in re.finditer(r'(\d+)', line)]])]
    infile.close()

# ***********************************************************************************

def escolheProximaCurvaFrente(connection):
    return connection.curva.tangent(0).getAngle()

# ***********************************************************************************

def escolheProximaCurvaAtras(connection):
    return connection.curva.tangent(1).getAngle()

# ***********************************************************************************

def idle(value):
    Player.move()
    for enemy in Enemies:
        enemy.move()
    verificaColisao()
    glutPostRedisplay()
    glutTimerFunc(FPS, idle, value)

# ***********************************************************************************

if __name__ == '__main__':
    criaCurvasDeArquivo()
    print("Pontos")
    [print(x) for x in Pontos]
    print("Ruas")
    [print(x) for x in BzCurvas]
    print("-----")
    CriaCurvas()

    pool = random.sample(range(len(BzCurvas)), ENEMIES + 1)
    random.shuffle(pool)

    Player.setStart(BzCurvas[pool.pop()], 0.5)
    print("Player")
    print(Player)
    print("-----")

    for enemy in Enemies:
        enemy.setStart(BzCurvas[pool.pop()], 0.5, random.random() > 0.5)
    print("Enemies")
    [print(x) for x in Enemies]
    print("-----")

    glutInit(sys.argv)
    glutInitDisplayMode(GLUT_RGBA)
    glutInitWindowSize(500, 500)
    glutInitWindowPosition(100, 100)
    wind = glutCreateWindow("Jogo das Curvas")
    glutDisplayFunc(display)
    glutReshapeFunc(reshape)
    glutKeyboardFunc(keyboard)
    glutSpecialFunc(arrow_keys)
    glutTimerFunc(FPS, idle, 0)

    try:
        glutMainLoop()
    except SystemExit:
        pass
